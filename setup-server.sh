#! /bin/bash

# link init file
cd /app/code
ln -s init.js init

# setup state folders
cd /app/state
mkdir files db && cd files
mkdir private public

# install server
jumpstart -Sy php5 nginx nodejs --noconfirm

# install composer
cd /app/code
curl -sS https://getcomposer.org/installer | php
mv composer.phar bin/composer

# prepare environment
cd /app/code
composer install

# move drush
cd /app/code/bin
ln -s /app/code/vendor/bin/drush.php drush

# get distro
cd /app/code
drush dl restaurant --drupal-project-rename=drupal
ln -s drupal public

# install drupal distro
cd /app/code/drupal
drush si install_configure_form.update_status_module='array(FALSE,FALSE)' -y --db-url=sqlite:/app/state/db/.ht.sqlite --account-name=admin --account-pass=admin

